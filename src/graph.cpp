#include "graph.h"
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <algorithm>
#include <map>
#include <stdexcept>

graph::graph()
{
  nodes = 0;
}

graph::~graph() { }

size_t graph::size() { return nodes; }

bool graph::hasEdge(size_t node0, size_t node1) {
  return (connect[node0].count(node1) > 0);
}

using size_pair = std::pair<size_t, size_t>;
bool comparePairs(const size_pair& lhs, const size_pair& rhs)
{
  return lhs.second < rhs.second;
}

int factorial(int n)
{
  return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

std::vector<size_t> graph::bijection(graph& other) {
  // test #1 - same number of nodes
  if(nodes != other.nodes) {
    std::cout << "Number of nodes is not equal!\n";
    return std::vector<size_t>(0);
  }
  // next function require not empty adjacency lists
  if(connect.size() == 0) return std::vector<size_t>(0);

  // test #2 - same numbers of nodes with same numbers of edges
  std::vector<size_pair> sizes0;
  std::vector<size_pair> sizes1;
  for(size_t i = 0; i < connect.size(); ++i) {
    sizes0.push_back(size_pair(i, connect[i].size()));
    sizes1.push_back(size_pair(i, other.connect[i].size()));
  }
  // will be used for permutations next, so requires keeping
  // indexes sorted within groups of the same degree
  std::stable_sort(sizes0.begin(), sizes0.end(), comparePairs);
  std::stable_sort(sizes1.begin(), sizes1.end(), comparePairs);

  // checking if there is the same number of nodes of the same degree
  for(size_t i = 0; i < nodes; ++i) {
    if(sizes0[i].second != sizes1[i].second) {
      std::cout << "Number of nodes of each degree is not equal!\n";
      return std::vector<size_t>(0);
    }
  }
  // now find subsets of nodes of the same degree
  std::vector<size_t> limit; // begin and end indexes for iterating
  size_t start = 0; // remember start of the current range
  limit.push_back(0);
  for(size_t i = 0; i < nodes; ++i)
    if(sizes0[start].second != sizes0[i].second)
      limit.push_back(start = i);
  limit.push_back(nodes);

  // numbers of permutations for each subset
  std::vector<size_t> perm(0);

  size_t n_perm = 1;
  for(size_t i = 0; i < limit.size() - 1; i++) {
    size_t f = factorial(limit[i+1] - limit[i]);
    n_perm *= f;
    perm.push_back(f);
  }
  // current permutation option for each index (lexicographical order)
  std::vector<size_t> curr(limit.size() - 1);

  // test - will store a current permutation to test
  // comp - node indexes in other to compare with test
  std::vector<size_t> test(nodes), comp(nodes);
  for(size_t i = 0; i < nodes; ++i) {
    test[i] = sizes0[i].first;
    comp[i] = sizes1[i].first;
  }

  // check all permutations of sub-sets
  for(size_t p = 0; p < n_perm; ++p) {
    // generating p-th permutation
    size_t tmp = p;
    size_t ind = 0;
    for(auto l : perm) {
      size_t n = tmp%l; // selected option for a parameter
      if(curr[ind] != n) // change needed in some range
        if(!std::next_permutation(test.begin()+limit[ind], test.begin()+limit[ind+1]))
          std::sort(test.begin()+limit[ind], test.begin()+limit[ind+1]); // cycle through permutations
      curr[ind++] = n;
      tmp /= l;
    }
    // now test contains a new hypothesis
    // check if each test[i] corresponds to comp[i]
    bool ok = true;
    for(size_t i = 0; i < nodes && ok; ++i) {
      // check if test[i] corresponds to comp[i]
      for(auto it : connect[test[i]]) {
        if(!other.hasEdge(comp[i], comp[std::distance(test.begin(),std::find(test.begin(),test.end(),it))])) {
          ok = false;
          break;
        }
      }
    }
    if(ok) {// return the first found solution
      std::vector<size_t> result(nodes);
      for(size_t i = 0; i < test.size(); ++i)
        result[test[i]] = comp[i];
      return result;
    }
  }
  return std::vector<size_t>(0);
}

std::istream& operator>> (std::istream& stream, graph& graph) {
  bool error = false;
  size_t a, b;
  std::string line;
  // read the number of nodes
  graph.nodes = 0;
  if(getline(stream,line)) {
    std::stringstream ss(line);
    if(!(ss >> a))
      throw std::invalid_argument("Incorrect graph size specification!\n");
    graph.nodes = a;
  } else throw std::invalid_argument("Incorrect graph size specification (2)!\n");

  graph_pair in;

  std::set<size_t> temp;
  for(size_t i = 0; i < graph.nodes; i++)
    graph.connect.push_back(temp);

  while(getline(stream, line)) {
    std::stringstream ss(line);
    if(!(ss >> a >> b)) {
      throw std::invalid_argument("Incorrect line: "+line);
    } else {
      if(a < 0 || b < 0 || a >= graph.nodes || b >= graph.nodes) {
        throw std::invalid_argument("Node indexes out of range!\n");
      } else {
        // a, b - a correct definition of a graph edge
        graph.connect[a].insert(b);
        graph.connect[b].insert(a);
      }
    }
  }
  if(error)
    graph.nodes = 0;
  return stream;
}

std::ostream& operator<< (std::ostream& stream, graph& graph) {
  std::cout << "[" << graph.nodes << "]\n";
  for(size_t i = 0; i < graph.nodes; ++i) {
    std::cout << i << ": ";
    for(auto it : graph.connect[i]) {
      std::cout << it << " ";
    }
    std::cout << "\n";
  }
  return stream;
}
