#ifndef GRAPH_H
#define GRAPH_H

#include <istream>
#include <ostream>
#include <vector>
#include <tuple>
#include <set>

using graph_pair = std::pair<size_t, size_t>;

class graph
{
  public:
    graph();
    ~graph();
    friend std::istream& operator>> (std::istream& stream, graph& graph);
    friend std::ostream& operator<< (std::ostream& stream, graph& graph);
    std::vector<size_t> bijection(graph& other);
    bool hasEdge(size_t node0, size_t node1);
    size_t size();
  protected:
  private:
    std::vector<graph_pair> data;
    size_t nodes;
    // adjacency list for graph nodes
    // vector of sets as multiple edges are not allowed
    std::vector<std::set<size_t> > connect;
};

std::istream& operator>> (std::istream& stream, graph& graph);
std::ostream& operator<< (std::ostream& stream, graph& graph);

#endif // GRAPH_H
