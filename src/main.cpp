#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include "graph.h"

using namespace std;

// operator overloaded for displaying bijection function
std::ostream& operator<< (std::ostream& stream, std::vector<size_t>& iso) {
  if(iso.size() > 0) {
    std::cout << "Isomorphic.\n";
    for(size_t i = 0; i < iso.size(); ++i)
      std::cout << i << " --> " << iso[i] << "\n";
  } else {
    std::cout << "Nonisomorphic.\n";
  }
  return stream;
}

int main(int argc, char* argv[])
{
  if(argc == 3) { // test isomorphism on two files
    vector<graph> graphs;
    graphs.resize(argc - 1);
    try {
      size_t i = 0;
      for(auto& graph : graphs) {
        std::ifstream ifs;
        ifs.open(argv[++i], std::ifstream::in);
        if(!ifs.good())
          throw std::invalid_argument("Error opening input file!");
        ifs >> graph;
      }
    }
    catch(const std::exception& e) {
      std::cout << e.what() << "\n\n";
      return -1;
    }

    std::vector<size_t> iso = graphs[0].bijection(graphs[1]);
    std::cout << iso << std::endl;
    //std::cout << graphs[0] << std::endl << graphs[1] << std::endl;
    return 0;
  } else if(argc == 1) {
    try {
      std::stringstream ss, ss1;
      std::string data = "5\n1 2\n2 3\n3 0\n";
      ss << data;
      ss1 << data;
      graph g, g1;
      ss >> g;
      ss1 >> g1;
      std::cout << "\nTEST #1: identical graphs"<< std::endl;
      std::vector<size_t> iso = g1.bijection(g);
      std::cout << iso;
      return 0;
    }
    catch(const std::exception& e) {
      std::cout << e.what() << "\n\n";
      return -1;
    }
  } else {
    std::cout << "Usage:\n  1) " << argv[0] << " <file1> <file2> - check graph isomorphism\n"
      << "  2) " << argv[0] << " - run hard-coded tests.\n\n";
  }
}
